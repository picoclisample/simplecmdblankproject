package info.picocli.examples;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import info.picocli.examples.service.SomeService;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Spec;
import picocli.CommandLine.Model.CommandSpec;


import java.util.List;
import java.util.concurrent.Callable;

/**
 *  実装のメインロジックを記載するクラス
 *  mycommand [-hV] [-x=<x>] [<positionals>...] [COMMAND]
 *  mycommand sub [-hV] [-y=<y>] [<positionals>...] [COMMAND]
 *  mycommand sub subsub [-hV] [-z=<z>]
 */
@Component
@Command(name = "mycommand", mixinStandardHelpOptions = true, subcommands = MyCommand.Sub.class)
public class MyCommand implements Callable<Integer> {

    @Option(names = "-x", description = "optional option")
    private String x;

    @Parameters(description = "positional params")
    private List<String> positionals;

    @Spec
    CommandSpec spec;

    /**
    * mycommand [-hV] [-x=<x>] [<positionals>...] [COMMAND]
    * の実装箇所
    */
    @Override
    public Integer call() {
        spec.commandLine().getOut().printf("mycommand was called with -x=%s and positionals: %s%n", x, positionals);
        return 23;
    }

    /**
     * mycommand sub [-hV] [-y=<y>] [<positionals>...] [COMMAND]
     * の実装箇所
     * 
     */
    @Component
    @Command(name = "sub", mixinStandardHelpOptions = true, subcommands = MyCommand.SubSub.class, exitCodeOnExecutionException = 34)
    static class Sub implements Callable<Integer> {
        @Option(names = "-y", description = "optional option")
        private String y;

        @Parameters(description = "positional params")
        private List<String> positionals;

        @Override
        public Integer call() {
            System.out.printf("mycommand sub was called with -y=%s and positionals: %s%n", y, positionals);
            return 33;
        }
    }
    
    /**
     * mycommand sub subsub [-hV] [-z=<z>]
     * の実装箇所
     * 
     */
    @Component
    @Command(name = "subsub", mixinStandardHelpOptions = true, exitCodeOnExecutionException = 44)
    static class SubSub implements Callable<Integer> {
        @Option(names = "-z", description = "optional option")
        private String z;

        @Autowired
        private SomeService service;

        @Override
        public Integer call() {
            System.out.printf("mycommand sub subsub was called with -z=%s. Service says: '%s'%n", z, service.service());
            return 43;
        }
    }    

}