package info.picocli.examples.service;

import org.springframework.stereotype.Service;

@Service
public class SomeService {

    /**
     * Example Service
     * @return "Hello world"
     */
    public String service() {

        System.out.println("Some Service is called");
        return "Hello world";
    }
    
}
