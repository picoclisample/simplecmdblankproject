package info.picocli.examples;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//import picocli.CommandLine;
//import picocli.CommandLine.IFactory;

/**
 * 
 * 
 * https://github.com/remkop/picocli/tree/main/picocli-spring-boot-starter
 * https://ksby.hatenablog.com/entry/2019/07/20/092721
 * 
 */
@SpringBootApplication
public class MySpringApp {

	public static void main(String[] args) {
		System.exit(SpringApplication.exit(SpringApplication.run(MySpringApp.class, args)));
	}

}