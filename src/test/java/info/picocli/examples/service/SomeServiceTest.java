package info.picocli.examples.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SomeServiceTest {
    @Nested
    @DisplayName("method : service")
    public class Service {
        private SomeService somService = new SomeService();

        @Test
        @DisplayName("正常に動作するテストケース")
        public void case1() throws Exception {

            assertEquals("Hello world",somService.service());
        }
        
    }

}
