package info.picocli.examples;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

import java.io.PrintWriter;
import java.io.StringWriter;

import picocli.CommandLine;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = NONE, classes = MySpringApp.class)
class SpringBootDemoApplicationTests {

	@Autowired
	CommandLine.IFactory factory;

	@Autowired
	MyCommand myCommand;


	@Test
	void contextLoads() {
	}

	@Nested
	@DisplayName("コマンドライン引数のテスト")
	public class testParsingCommandLineArgs {

		@Test
		@DisplayName("ag")
		public void case1() throws Exception {
			CommandLine.ParseResult parseResult = new CommandLine(myCommand, factory)
					.parseArgs("-x", "abc", "sub", "-y", "123");
			assertEquals("abc", ReflectionTestUtils.getField(myCommand,"x") );
			
			assertNull(ReflectionTestUtils.getField(myCommand,"positionals"));
			assertTrue(parseResult.hasSubcommand());

			CommandLine.ParseResult subResult = parseResult.subcommand();
			MyCommand.Sub sub = (MyCommand.Sub) subResult.commandSpec().userObject();

			assertEquals("123", ReflectionTestUtils.getField(sub,"y"));
			assertNull(ReflectionTestUtils.getField(sub, "positionals"));

		}

	}

	@Nested
	@DisplayName("利用方法の説明のテスト")
	public class testUsageHelp {

		@Test
		public void case1() {
			String expected = String.format("" +
            	"Usage: mycommand [-hV] [-x=<x>] [<positionals>...] [COMMAND]%n" +
				"      [<positionals>...]   positional params%n" +
	            "  -h, --help               Show this help message and exit.%n" +
    	        "  -V, --version            Print version information and exit.%n" +
        	    "  -x=<x>                   optional option%n" +
            	"Commands:%n" +
            	"  sub%n");

        	String actual = new CommandLine(myCommand, factory).getUsageMessage(CommandLine.Help.Ansi.OFF);
        	assertEquals(expected, actual);
			
		}

	}

	@Nested
	@DisplayName("実行結果のテスト")
	public class testExecute {

		@Test
		public void case1() {


			// black box testing
			CommandLine cmd = new CommandLine(myCommand, factory);
			StringWriter sw = new StringWriter();
			cmd.setOut(new PrintWriter(sw));
			
			int exitCode = cmd.execute("-x=123");

			assertEquals(23, exitCode);
			assertEquals("mycommand was called with -x=123 and positionals: null\n", sw.toString());
		}
	

	}

}
