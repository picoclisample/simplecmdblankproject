package info.picocli.examples;

// 標準出力、標準エラー出力をテストするためのインポート
//import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemErr;
//import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOutNormalized;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyCommandTest {


    @Nested
    @DisplayName("method : call")
    public class Call {

        private MyCommand test = new MyCommand();

        @Test
        @DisplayName("正常に動作するテストケース")
        public void case1() throws Exception {

            ReflectionTestUtils.setField(test, "x", "1");

            assertEquals(23, test.call());
        }

    }

}