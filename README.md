# SimpleCmdBlankProject
picocliを使うためのサンプル

https://github.com/remkop/picocli/tree/main/picocli-spring-boot-starter
をもとにしている

オプションは以下の通り
```shell-session{.line-numbers}
 mycommand [-hV] [-x=<x>] [<positionals>...] [COMMAND]
 mycommand sub [-hV] [-y=<y>] [<positionals>...] [COMMAND]
 mycommand sub subsub [-hV] [-z=<z>]
```



## Test and Deploy
以下のセットアップは別途実施する

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

